package com.example.examapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ExamSession {
    SharedPreferences pref, autocompletepref;
    SharedPreferences.Editor editor, autocompeditor;
    static Context _context;
    int PRIVATE_MODE = 0;
    public static final String PREF_NAME = "EXAMQCSPref";
    public static final String AUTO_PREF_NAME = "EXAMSQCSPref";
    public static final String EXAM_ARRAY_STRING = "Exam_ARRAY_STRING";

    public static JSONArray EXAM_ARRAY = new JSONArray();
    private Activity activity;
    public static final String SHOWCASE = "showcase";

    @SuppressLint("CommitPrefEdits")
    public ExamSession(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        autocompletepref = _context.getSharedPreferences(AUTO_PREF_NAME, PRIVATE_MODE);
        autocompeditor = autocompletepref.edit();
    }

    public ExamSession(Activity activity) {
        this.activity = activity;
        this._context = activity.getApplicationContext();
        pref = activity.getApplicationContext().getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        autocompletepref = _context.getSharedPreferences(AUTO_PREF_NAME, PRIVATE_MODE);
        autocompeditor = autocompletepref.edit();
    }

    public SharedPreferences.Editor getSessionEditor() {
        return editor;
    }

    public void setShowcase(String showcase) {
        editor.putString(SHOWCASE, showcase).commit();
    }

    public String getExamjsonarray() {
        return pref.getString(EXAM_ARRAY_STRING, null);
    }

    public Boolean isInExam(String id) throws JSONException {
        Boolean status = false;
        for (Integer i = 0; i < EXAM_ARRAY.length(); i++) {
            JSONObject ned = EXAM_ARRAY.getJSONObject(i);
            if (ned.getString("tlq_id").equals(id)) ;
            {
                status = true;
            }
        }
        return status;
    }

    public void createExamSession(JSONArray responseJSONObject) {
        try {
            EXAM_ARRAY = responseJSONObject;
            editor.putString(EXAM_ARRAY_STRING, EXAM_ARRAY + "");
            editor.commit();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
}
