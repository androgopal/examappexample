package com.example.examapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.examapp.databinding.ActivityMainBinding;
import com.example.examapp.studentexam.ResultActivity;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    private int EXTERNAL_STORAGE_PERMISSION_CODE = 23;
    ActivityMainBinding binding;
    ProgressDialog pDialog;
    JSONObject language_question;
    String exam_question = "", q_solution = "", headerTitle = "", correctOpt = "";
    PagerAdapter adapter;
    JSONArray jsonArray = new JSONArray();
    JSONArray question_list = new JSONArray();
    ExamSession examSession;
    JSONArray exam_array = new JSONArray();
    JSONObject examObject;
    String examdata1 = "", tlq_option_attach1 = "", tlq_option_attach2 = "";
    static int tlq_option_attach = 0;

    @SuppressLint({"WrongConstant", "LongLogTag"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.setStatusBar(MainActivity.this);
        // Requesting Permission to access External Storage
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                EXTERNAL_STORAGE_PERMISSION_CODE);
        pDialog = Constant.getProgressBar(MainActivity.this);
        examSession = new ExamSession(MainActivity.this);
        try {
            if (examSession.getExamjsonarray() != null) {
                exam_array = new JSONArray(examSession.getExamjsonarray());
                Constant.logPrint("examSession123", exam_array + "\n" + exam_array.length());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        binding.time.setOnClickListener(v -> {
            savePublicly(binding.getRoot());
        });
        current_time();

        binding.llReview.setOnClickListener(v -> {
            binding.llReview1.setVisibility(View.VISIBLE);
            binding.llReview.setVisibility(View.GONE);
        });
        binding.llReview1.setOnClickListener(v -> {
            binding.llReview1.setVisibility(View.GONE);
            binding.llReview.setVisibility(View.VISIBLE);
        });
        binding.imgLang.setOnClickListener(v -> {
            final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(MainActivity.this);
            bottomSheetDialog.setContentView(R.layout.lang_layout);
            bottomSheetDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//            TextView btn_buy = bottomSheetDialog.findViewById(R.id.btn_buy);
//            TextView tv_go_inside = bottomSheetDialog.findViewById(R.id.tv_go_inside);
            bottomSheetDialog.show();
        });
        binding.imgMenu.setOnClickListener(view -> {
            if (!binding.drawelayout.isDrawerOpen(GravityCompat.END))
                binding.drawelayout.openDrawer(Gravity.END);
            else binding.drawelayout.closeDrawer(Gravity.END);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
            binding.dialogRecycler.setLayoutManager(linearLayoutManager);
            PaperAdapter paperAdapter = new PaperAdapter(getApplicationContext(), jsonArray);
            binding.dialogRecycler.setAdapter(paperAdapter);
        });
        binding.previous.setOnClickListener(v -> {
            binding.viewpager.setCurrentItem(getItemm(1), true); //getItem(-1) for previous
        });
        binding.llPay.setOnClickListener(v -> {
            exit_exam_box();
        });
        binding.submittest.setOnClickListener(v -> {
            submit_Box();
        });

       

        binding.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (positionOffsetPixels == 0) {
                    //Do something on selected page at position

                    binding.tvquestionNo.setText("Q." + String.valueOf(position + 1));
                    Toast.makeText(MainActivity.this, "Position :- " + (position + 1), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // exam_details();
        addItemsFromJSON();
        /*binding.next.setOnClickListener(v -> {
            try {


                if (!examSession.isInExam(language_question.getString("tlq_id"))) {
                    JSONObject obj = new JSONObject();
                    obj.put("tlq_id", language_question.getString("tlq_id"));
                    obj.put("tlq_question_id", language_question.getString("tlq_question_id"));
                    obj.put("tlq_unique_id", language_question.getString("tlq_unique_id"));
                    obj.put("tlq_question_type", language_question.getString("tlq_question_type"));
                    obj.put("tlq_options", language_question.getString("tlq_options"));
                    obj.put("tlq_option1_attachments", language_question.getString("tlq_option1_attachments"));
                    obj.put("tlq_lang_id", language_question.getString("tlq_lang_id"));
                    obj.put("tlq_question_text", language_question.getString("tlq_question_text"));
                    obj.put("tlq_question_text_hindi", language_question.getString("tlq_question_text_hindi"));
                    obj.put("tlq_options_hindi", language_question.getString("tlq_options_hindi"));
                    obj.put("tlq_question_attachments", language_question.getString("tlq_question_attachments"));
                    obj.put("tlq_option2_attachments", language_question.getString("tlq_option2_attachments"));
                    obj.put("tlq_option3_attachments", language_question.getString("tlq_option3_attachments"));
                    obj.put("tlq_option4_attachments", language_question.getString("tlq_option4_attachments"));
                    obj.put("tlq_option5_attachments", language_question.getString("tlq_option5_attachments"));
                    obj.put("tlq_option1_attachments_hindi", language_question.getString("tlq_option1_attachments_hindi"));
                    obj.put("tlq_option2_attachments_hindi", language_question.getString("tlq_option2_attachments_hindi"));
                    obj.put("tlq_option3_attachments_hindi", language_question.getString("tlq_option3_attachments_hindi"));
                    obj.put("tlq_option4_attachments_hindi", language_question.getString("tlq_option4_attachments_hindi"));
                    obj.put("tlq_option5_attachments_hindi", language_question.getString("tlq_option5_attachments_hindi"));
                    obj.put("tlq_question_type_hindi", language_question.getString("tlq_question_type_hindi"));
                    obj.put("tlq_question_attachments_hindi", language_question.getString("tlq_question_attachments_hindi"));
                    obj.put("tlq_english_instruction", language_question.getString("tlq_english_instruction"));
                    obj.put("tlq_hindi_instruction", language_question.getString("tlq_hindi_instruction"));
                    obj.put("tlq_hindi_solution", language_question.getString("tlq_hindi_solution"));
                    obj.put("tlq_english_solution", language_question.getString("tlq_english_solution"));
                    obj.put("tlq_hindi_solution_attach", language_question.getString("tlq_hindi_solution_attach"));
                    obj.put("tlq_english_solution_attach", language_question.getString("tlq_english_solution_attach"));
                    obj.put("tlq_question_status", language_question.getString("tlq_question_status"));
                    obj.put("tlq_question_order", language_question.getString("tlq_question_order"));
                    obj.put("tlq_user_id", language_question.getString("tlq_user_id"));
                    obj.put("tlq_option_attach", language_question.getInt(tlq_option_attach+""));
                    obj.put("tlq_option_hindi_attach", language_question.getString("tlq_option_hindi_attach"));
                    obj.put("tlq_sub_id", language_question.getString("tlq_sub_id"));
                    obj.put("tlq_correct_option", language_question.getString("tlq_correct_option"));

                    exam_array.put(obj);
                    examSession.createExamSession(exam_array);
                    Log.e("exam_arrayexam_arrayexam_arrayexam_array", exam_array.length() + "");
                    Log.e("exam_arrayexam_arrayexam1", tlq_option_attach + "");
                }


//                for(Integer i=0;i<question_list.length();i++) {
//                    JSONObject ned = question_list.getJSONObject(i);
//                    JSONObject itemm = ned.getJSONObject("ot_test_language_question");
//                    if(!examSession.isInExam(itemm.getString("tlq_id"),itemm.getString("tlq_options"),itemm.getString("tlq_question_status"))) {
//                        JSONObject obj=new JSONObject();
//                        obj.put("tlq_id", itemm.getString("tlq_id"));
//                        obj.put("tlq_options", itemm.getString("tlq_options"));
//                        obj.put("tlq_question_status", itemm.getString("tlq_question_status"));
//                        exam_array.put(obj);
//                        examSession.createExamSession(exam_array);
//                    }
//                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            binding.viewpager.setCurrentItem(getItem(1), true); //getItem(-1) for previous

        });*/

        binding.next.setOnClickListener(v -> {

            try {
                String jsonDataString = getdata();
                JSONObject jsonObject = new JSONObject(jsonDataString);
                JSONObject exam_data = jsonObject.getJSONObject("rowExamInfo");
                jsonArray = exam_data.getJSONArray("ot_user_test_subject_view");
                for (int i = 0; i < jsonArray.length(); i++) {
                    examObject = jsonArray.getJSONObject(i);
                    question_list = examObject.getJSONArray("ot_question");
                    for (int n = 0; n < question_list.length(); n++) {
                        JSONObject jobject = question_list.getJSONObject(n);
                        jobject.put("tlq_option_attach", tlq_option_attach1);
                        question_list.put(n, jobject);
                        if (jobject.has("ot_test_language_question")) {
                            language_question = jobject.getJSONObject("ot_test_language_question");
//                            JSONObject obj = new JSONObject(String.valueOf(language_question));
//                            obj.put("tlq_option_attach", tlq_option_attach1);
//                            jsonArray.put(n, obj);
//                            Constant.logPrint("objobjobjobjobj", obj + "");
//                       exam_question= language_question.getString("tlq_question_text");
                            // q_solution=language_question.getString("tlq_hindi_solution");
//                        data = language_question.getString("tlq_options");
//                       items = new ArrayList<String>(Arrays.asList(data.split("###OPT###")));
                        }
                    }

                }


            } catch (JSONException | IOException e) {
                ///   Log.d(TAG, "addItemsFromJSON: ", e);
            }


           /* for(Integer i=0;i<question_list.length();i++) {
                try {
                    JSONObject obj = new JSONObject(String.valueOf(language_question));
                    obj.put("tlq_option_attach", tlq_option_attach1);
                    Constant.logPrint("objobjobjobjobj", obj+"");
                    }
                 catch (JSONException e) {
                    e.printStackTrace();
                }

                }*/
            binding.viewpager.setCurrentItem(getItem(1), true); //getItem(-1) for previous
        });
    }

    private void current_time() {
        final Calendar t = Calendar.getInstance();
        binding.time.setText(DateFormat.getTimeFormat(this/*Context*/).format(t.getTime()));
    }

    @SuppressLint("WrongConstant")
    private void submit_Box() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.test_submit_box, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        Button btn_cancle = dialogView.findViewById(R.id.btn_cancle);
        Button btn_submit = dialogView.findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), ResultActivity.class);
            startActivity(intent);
        });
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        btn_cancle.setOnClickListener(v -> {
            alertDialog.dismiss();
        });
    }

    private void exit_exam_box() {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.exit_exam_box);
        TextView exam_cancle = dialog.findViewById(R.id.exam_cancle);
        TextView exam_submit = dialog.findViewById(R.id.exam_submit);
//        btn_submit.setOnClickListener(v -> {
//            Intent intent=new Intent(getApplicationContext(), ResultActivity.class);
//            startActivity(intent);
//        });
        dialog.show();
        exam_cancle.setOnClickListener(v -> {
            dialog.dismiss();
        });
    }

    private int getItem(int i) {
        return binding.viewpager.getCurrentItem() + i;
    }

    private int getItemm(int i) {
        return binding.viewpager.getCurrentItem() - i;
    }

    public void savePublicly(View view) {

        String editTextData = null;
        try {
            editTextData = readJSONDataFromFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        String uniqueString = UUID.randomUUID().toString();
        File file = new File(folder, "uniqueString.json");
        writeTextData(file, editTextData);
        addItemsFromJSON();
        // editText.setText("");
    }


    private void writeTextData(File file, String data) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(data.getBytes());
            Toast.makeText(this, "Done" + file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @SuppressLint("LongLogTag")
    private void addItemsFromJSON() {
        try {
            String jsonDataString = getdata();
            JSONObject jsonObject = new JSONObject(jsonDataString);
            JSONObject exam_data = jsonObject.getJSONObject("rowExamInfo");
            binding.totalQ.setText("Total Q:" + " " + exam_data.getString("test_total_question"));
            //binding.time.setText(exam_data.getString("test_duration"));

            jsonArray = exam_data.getJSONArray("ot_user_test_subject_view");
            for (int i = 0; i < jsonArray.length(); i++) {
                examObject = jsonArray.getJSONObject(i);
                binding.txtTitle.setText(examObject.getString("sub_name"));
                binding.totalMarks.setText("+" + examObject.getString("tsd_total_marks"));
                binding.negativeMarks.setText("-" + examObject.getString("tsd_negative_marks"));
                question_list = examObject.getJSONArray("ot_question");

                for (int n = 0; n < question_list.length(); n++) {
                    JSONObject jobject = question_list.getJSONObject(n);
                    if (jobject.has("ot_test_language_question")) {
                        language_question = jobject.getJSONObject("ot_test_language_question");
//                       exam_question= language_question.getString("tlq_question_text");
//                         q_solution=language_question.getString("tlq_hindi_solution");
//                        data = language_question.getString("tlq_options");
//                       items = new ArrayList<String>(Arrays.asList(data.split("###OPT###")));
                    }
                }

            }
            adapter = new SliderAdapter(getApplicationContext(), question_list);
            binding.viewpager.setAdapter(adapter);
            binding.viewpager.setCurrentItem(0, false);


        } catch (JSONException | IOException e) {
            ///   Log.d(TAG, "addItemsFromJSON: ", e);
        }
    }

    /*public void showPublicData() {
        // Accessing the saved data from the downloads folder
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        // geeksData represent the file data that is saved publicly
        File file = new File(folder, "uniqueString.json");
        String data = getdata(file);
        if (data != null) {
            Constant.setToast(getApplicationContext(), "File Found");
        } else {
            Constant.setToast(getApplicationContext(), "No File Found");
        }
    }*/

    private String getdata() throws IOException {
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        FileInputStream inputStream = null;
        StringBuilder builder = new StringBuilder();
        // geeksData represent the file data that is saved publicly
        File file = new File(folder, "uniqueString.json");
        try {

            String jsonString = null;
            inputStream = new FileInputStream(file);
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream, "UTF-8"));

            while ((jsonString = bufferedReader.readLine()) != null) {
                builder.append(jsonString);
                Constant.logPrint("fjfjfjfffjfjf", jsonString + "");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return new String(builder);
    }

    private String readJSONDataFromFile() throws IOException {
        InputStream inputStream = null;
        StringBuilder builder = new StringBuilder();

        try {

            String jsonString = null;
            inputStream = getResources().openRawResource(R.raw.exam_data);
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream, "UTF-8"));

            while ((jsonString = bufferedReader.readLine()) != null) {
                builder.append(jsonString);
                Constant.logPrint("fjfjfjfffjfjf", jsonString + "");
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return new String(builder);
    }


    public class RecyclerAdapter1 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private Context context;
        JSONObject itemm;
        ArrayList<String> item;
        String exam_question;
        String q_solution;
        String headerTitle;
        String correctOpt;
        private int row_index;
        private static final int TYPE_HEADER = 0;
        private static final int TYPE_FOOTER = 1;
        private static final int TYPE_ITEM = 2;
        boolean iscolor = true;


        public RecyclerAdapter1(Context context, JSONObject itemm, ArrayList<String> item, String exam_question, String q_solution, String headerTitle, String correctOpt) {
            this.context = context;
            this.itemm = itemm;
            this.item = item;
            this.exam_question = exam_question;
            this.q_solution = q_solution;
            this.headerTitle = headerTitle;
            this.correctOpt = correctOpt;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == TYPE_ITEM) {
                //Inflating recycle view item layout
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ques, parent, false);
                return new RecyclerAdapter1.ItemViewHolder(itemView);
            } else if (viewType == TYPE_HEADER) {
                //Inflating header view
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_header, parent, false);
                return new RecyclerAdapter1.HeaderViewHolder(itemView);
            } else if (viewType == TYPE_FOOTER) {
                //Inflating footer view
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_footer, parent, false);
                return new RecyclerAdapter1.FooterViewHolder(itemView);
            } else return null;

        }

        @SuppressLint({"LongLogTag", "SetJavaScriptEnabled"})
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

            if (holder instanceof RecyclerAdapter1.HeaderViewHolder) {
                RecyclerAdapter1.HeaderViewHolder headerHolder = (RecyclerAdapter1.HeaderViewHolder) holder;

                headerHolder.header_webview.clearFormData();
                headerHolder.header_webview.clearCache(true);
                headerHolder.header_webview.getSettings().setJavaScriptEnabled(true);
                headerHolder.header_webview.getSettings().setDomStorageEnabled(true);
                headerHolder.header_webview.clearHistory();
                headerHolder.header_webview.loadData(exam_question, "text/html", "UTF-8");
                headerHolder.headerTitle.setText(headerTitle);

            } else if (holder instanceof RecyclerAdapter1.FooterViewHolder) {
                RecyclerAdapter1.FooterViewHolder footerHolder = (RecyclerAdapter1.FooterViewHolder) holder;
                footerHolder.footerText.setText(correctOpt);

            } else if (holder instanceof RecyclerAdapter1.ItemViewHolder) {
                RecyclerAdapter1.ItemViewHolder itemViewHolder = (RecyclerAdapter1.ItemViewHolder) holder;
                itemViewHolder.txt_option.setText(item.get(position - 1));
                int j = Integer.parseInt(correctOpt);

                //Constant.logPrint("correctOpt", correctOpt);

                if (position == 1) {
                    itemViewHolder.tvindex.setText("A");
                } else if (position == 2) {
                    itemViewHolder.tvindex.setText("B");
                } else if (position == 3) {
                    itemViewHolder.tvindex.setText("C");
                } else if (position == 4) {
                    itemViewHolder.tvindex.setText("D");
                } else if (position == 5) {
                    itemViewHolder.tvindex.setText("E");
                }
                if (position == j) {
                    itemViewHolder.tvindex.setBackgroundResource(R.drawable.green_circle);
                    itemViewHolder.ll_option.setBackgroundResource(R.drawable.correct_answer_layout);
                    //itemViewHolder.ll_index.setBackgroundResource(R.drawable.green_circle);
                }
                itemViewHolder.ll_option.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        row_index = position;
                        notifyItemRangeChanged(1, item.size());
                        int ChAnswer = row_index;
                        Constant.logPrint("ChAnswer", ChAnswer + "");
                        tlq_option_attach = row_index;
                    }
                });
                if (row_index == position) {
                    tlq_option_attach1 = String.valueOf(row_index);
                    itemViewHolder.ll_option.setBackgroundResource(R.drawable.exam_layout_background);
                    itemViewHolder.ll_index.setBackgroundResource(R.drawable.green_circle);

                } else if (row_index != position) {
                    itemViewHolder.ll_option.setBackgroundResource(R.drawable.white_layout_background);
                    itemViewHolder.ll_index.setBackgroundResource(R.drawable.grey_circle);
                }
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0) {
                return TYPE_HEADER;
            } else if (position == item.size() + 1) {
                return TYPE_FOOTER;
            }
            return TYPE_ITEM;
        }

        @Override
        public int getItemCount() {
            return item.size() + 2;
        }

        private class HeaderViewHolder extends RecyclerView.ViewHolder {
            TextView headerTitle;
            WebView header_webview;

            public HeaderViewHolder(View view) {
                super(view);
                headerTitle = (TextView) view.findViewById(R.id.txt_headerTitle);
                header_webview = (WebView) view.findViewById(R.id.header_webview);
            }
        }

        private class FooterViewHolder extends RecyclerView.ViewHolder {
            TextView footerText;

            public FooterViewHolder(View view) {
                super(view);
                footerText = (TextView) view.findViewById(R.id.footer_text);
            }
        }

        private class ItemViewHolder extends RecyclerView.ViewHolder {
            TextView txt_option, txt_option1, tvindex;
            CardView ll_option, ll_option1;
            LinearLayout ll_index;

            public ItemViewHolder(View itemView) {
                super(itemView);
                tvindex = itemView.findViewById(R.id.tvindex);
                txt_option = (TextView) itemView.findViewById(R.id.txt_option);
                ll_option = (CardView) itemView.findViewById(R.id.ll_option);
                ll_index = (LinearLayout) itemView.findViewById(R.id.ll_index);
            }
        }
    }

    public class SliderAdapter extends PagerAdapter {
        private LayoutInflater inflater;
        private Context context;
        JSONArray iteam;

        public SliderAdapter(Context context, JSONArray iteam) {
            this.context = context;
            this.iteam = iteam;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public int getCount() {
            return question_list.length();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @SuppressLint("LongLogTag")
        @Override
        public Object instantiateItem(ViewGroup view, int position) {
            View myImageLayout = inflater.inflate(R.layout.exam_slider_layout_item, view, false);
            // Toast.makeText(getApplicationContext(), "Position :- "+position, Toast.LENGTH_SHORT).show();
            RecyclerView recyler_pager = (RecyclerView) myImageLayout.findViewById(R.id.recyler_pager);
            int Qno = position;
            //binding.tvquestionNo.setText("Q." + Qno);
            Log.e("QnoQnoQnoQnoQno", String.valueOf(Qno));
            try {
                JSONObject item = question_list.getJSONObject(position);
                Constant.logPrint("gkgkgkgkgkkgkgkgkggkgkggk", item + "");
                JSONObject itemm = item.getJSONObject("ot_test_language_question");
                exam_question = itemm.getString("tlq_question_text");
                headerTitle = itemm.getString("tlq_id");
                q_solution = itemm.getString("tlq_hindi_solution");
                correctOpt = itemm.getString("tlq_correct_option");
                String data = itemm.getString("tlq_options");
                ArrayList<String> items = new ArrayList<String>(Arrays.asList(data.split("###OPT###")));
                LinearLayoutManager lm = new LinearLayoutManager(getApplicationContext());
                lm.setOrientation(LinearLayoutManager.VERTICAL);
                recyler_pager.setLayoutManager(lm);
                RecyclerAdapter1 recyclerAdapter1 = new RecyclerAdapter1(getApplicationContext(), item, items, exam_question, q_solution, headerTitle, correctOpt);
                recyler_pager.setAdapter(recyclerAdapter1);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            view.addView(myImageLayout);
            return myImageLayout;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

    }


    public class PaperAdapter extends RecyclerView.Adapter<PaperAdapter.ViewHolder> {
        JSONArray listItem;
        private Context mContext;

        public PaperAdapter(Context context, JSONArray listItem) {
            this.mContext = context;
            this.listItem = listItem;

        }

        @NonNull
        @Override
        public PaperAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_menu_ques, viewGroup, false);
            return new PaperAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull PaperAdapter.ViewHolder holder, int position) {
            try {
                JSONObject item = listItem.getJSONObject(position);
                holder.txt_ques.setText(item.getString("sub_name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.setIsRecyclable(false);

            holder.im_arrow_down.setOnClickListener(v -> {
                holder.im_arrrow_up.setVisibility(View.VISIBLE);
                holder.sub_recyler.setVisibility(View.GONE);
                holder.im_arrow_down.setVisibility(View.GONE);
            });
            holder.im_arrrow_up.setOnClickListener(v -> {
                holder.im_arrow_down.setVisibility(View.VISIBLE);
                holder.sub_recyler.setVisibility(View.VISIBLE);
                holder.im_arrrow_up.setVisibility(View.GONE);
            });
        }

        @Override
        public int getItemCount() {
            return listItem.length();

        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView im_arrow_down, im_arrrow_up;
            TextView txt_ques;
            RecyclerView sub_recyler;

            public ViewHolder(@NonNull View view) {
                super(view);
                txt_ques = view.findViewById(R.id.txt_ques);
                im_arrow_down = view.findViewById(R.id.im_arrow_down);
                im_arrrow_up = view.findViewById(R.id.im_arrrow_up);
                sub_recyler = view.findViewById(R.id.sub_recyler);
                GridLayoutManager lm = new GridLayoutManager(mContext, 5);
                sub_recyler.setLayoutManager(lm);

                ExamListAdapter examListAdapter = new ExamListAdapter(mContext, question_list);
                sub_recyler.setAdapter(examListAdapter);
            }
        }

        public class ExamListAdapter extends RecyclerView.Adapter<ExamListAdapter.ViewHolder> {
            Context context;
            JSONArray questionLists;


            public ExamListAdapter(Context context, JSONArray questionLists) {
                this.context = context;
                this.questionLists = questionLists;
                //this.answerLists=answerLists;
            }

            @NonNull
            @Override
            public ExamListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.exam_list_layout, viewGroup, false);
                return new ExamListAdapter.ViewHolder(view);
            }

            @SuppressLint({"WrongConstant", "LongLogTag"})
            @Override
            public void onBindViewHolder(@NonNull ExamListAdapter.ViewHolder holder, int position) {
                int d = ++position;
                holder.exam_ques.setText(d + "");
                Constant.logPrint("dposition", d + "");
                int finalPosition = position;
                holder.ll_exam_ques.setOnClickListener(v -> {
                    binding.viewpager.setCurrentItem(finalPosition - 1);
                    Constant.logPrint("finalPosition", finalPosition + "");
                    if (binding.drawelayout.isDrawerOpen(Gravity.END)) {
                        binding.drawelayout.closeDrawer(Gravity.END);
                    } else {
                        binding.drawelayout.openDrawer(Gravity.END);
                    }
                });


                try {
                    if (examSession.getExamjsonarray() != null) {
                        exam_array = new JSONArray(examSession.getExamjsonarray());
                        examdata1 = exam_array + "";
                        JSONObject jsonObject = new JSONObject();
                        Log.e("carcartcartcartcartcarttexam_array", exam_array.length() + "");
                        Log.d("this is my deep array", "deep arr: " + (examdata1));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public int getItemCount() {
                return questionLists.length();
            }

            public class ViewHolder extends RecyclerView.ViewHolder {
                TextView exam_ques;
                LinearLayout ll_exam_ques;

                public ViewHolder(@NonNull View view) {
                    super(view);
                    ll_exam_ques = view.findViewById(R.id.ll_exam_ques);
                    exam_ques = view.findViewById(R.id.exam_ques);
                }
            }

        }
    }
}