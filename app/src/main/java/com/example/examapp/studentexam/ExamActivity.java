package com.example.examapp.studentexam;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.examapp.Constant;
import com.example.examapp.MainActivity;
import com.example.examapp.R;
import com.example.examapp.database.Entity.QuestionTable;
import com.example.examapp.database.Entity.RawExamInfoTable;
import com.example.examapp.database.Entity.SubjectDataTable;
import com.example.examapp.databinding.ActivityExamBinding;
import com.example.examapp.databinding.ActivityMainBinding;
import com.example.examapp.databinding.ItemRowQuestionLayoutBinding;
import com.example.examapp.studentexam.adapter.QuestionAdapter;
import com.example.examapp.viewmodel.model.Viewmodel;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ExamActivity extends AppCompatActivity implements QuestionAdapter.QuestionItem {
    static ActivityExamBinding binding;
    Viewmodel mViewModel;
    private Activity activity;
    String examTime = "";
    private int quesId;
    private static int queNumPosition;
    private QuestionAdapter.QuestionItem questionItem;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityExamBinding.inflate(getLayoutInflater());
        activity = this;
        questionItem = this;
        setContentView(binding.getRoot());
        mViewModel = new ViewModelProvider(this).get(Viewmodel.class);
        Constant.setStatusBar(ExamActivity.this);


        // recycler scroll
        setSnapHelper();
        binding.imgMenu.setOnClickListener(view -> {
            if (!binding.drawelayout.isDrawerOpen(GravityCompat.END))
                binding.drawelayout.openDrawer(Gravity.END);
            else binding.drawelayout.closeDrawer(Gravity.END);
        });
        try {
            //mViewModel.DeleteAll();
            mViewModel.GetExamDataFromApi(readJSONDataFromFile());
        } catch (IOException e) {
            Toast.makeText(activity, "Invalid Json", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        binding.llPay.setOnClickListener(v -> {
            exit_exam_box();
        });
        binding.submittest.setOnClickListener(v -> {
            submit_Box();
        });
        binding.imgLang.setOnClickListener(v -> {
            final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(ExamActivity.this);
            bottomSheetDialog.setContentView(R.layout.lang_layout);
            bottomSheetDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            TextView tv_english = bottomSheetDialog.findViewById(R.id.tv_english);
            TextView tv_hindi = bottomSheetDialog.findViewById(R.id.tv_hindi);
            TextView tv_Cancle = bottomSheetDialog.findViewById(R.id.tv_Cancle);
            bottomSheetDialog.show();
            tv_Cancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bottomSheetDialog.dismiss();
                }
            });
        });
        mViewModel.getExamData().observe(this, new Observer<RawExamInfoTable>() {
            @Override
            public void onChanged(RawExamInfoTable rawExamInfoTable) {

                if (rawExamInfoTable != null) {
                    // Log.d("liveData", rawExamInfoTable.getSubId() + "");
                    binding.txtTitle.setText(rawExamInfoTable.getTestTitle());
                    binding.totalQ.setText("Total Q: " + rawExamInfoTable.getSubQuestion());
                    binding.negativeMarks.setText("-" + rawExamInfoTable.getNegativeMarks());
                    binding.totalMarks.setText("+" + rawExamInfoTable.getTestMarks());
                    examTime = rawExamInfoTable.getTestDuration();
                    startTimer();
                    mViewModel.getSubjectata(rawExamInfoTable.getTestId()).observe((ExamActivity) activity, new Observer<List<SubjectDataTable>>() {
                        @Override
                        public void onChanged(List<SubjectDataTable> subjectDataTables) {

                            if (subjectDataTables != null && subjectDataTables.size() > 0) {
                                Log.d("liveData", subjectDataTables.get(0).getSubjectName() + "" + subjectDataTables.size());


                                mViewModel.getQuestiontata(rawExamInfoTable.getSubId()).observe((ExamActivity) activity, new Observer<List<QuestionTable>>() {
                                    @Override
                                    public void onChanged(List<QuestionTable> questionTables) {
                                        if (questionTables != null) {
                                            //Log.d("liveData1", questionTables.size()+"++"+questionTables.get(1).getUserQueStatus());
                                            /*for(int i=0; i<=questionTables.size(); i++) {
                                                Log.d("values : ", questionTables.get(i).getQuestionHeaderHindi() +" +++ "+i+ questionTables.get(i).getUserQueStatus());
                                            }*/
                                            binding.previous.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    if (quesId > 0) {
                                                        binding.questionRecycler.smoothScrollToPosition(quesId - 1);

                                                    }
                                                }
                                            });
                                            binding.next.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    if (quesId < questionTables.size() - 1) {
                                                        binding.questionRecycler.smoothScrollToPosition(quesId + 1);
                                                    }
                                                }
                                            });

                                            LinearLayoutManager lm = new LinearLayoutManager(getApplicationContext());
                                            lm.setOrientation(LinearLayoutManager.HORIZONTAL);
                                            binding.questionRecycler.setLayoutManager(lm);
                                            QuestionAdapter questionAdapter = new QuestionAdapter(getApplicationContext(), questionTables, questionItem);
                                            binding.questionRecycler.setAdapter(questionAdapter);


                                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                                            binding.dialogRecycler.setLayoutManager(linearLayoutManager);
                                            PaperAdapter paperAdapter = new PaperAdapter(getApplicationContext(), subjectDataTables, questionTables);
                                            binding.dialogRecycler.setAdapter(paperAdapter);
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }

    private void exit_exam_box() {
        final Dialog dialog = new Dialog(ExamActivity.this);
        dialog.setContentView(R.layout.exit_exam_box);
        TextView exam_cancle = dialog.findViewById(R.id.exam_cancle);
        TextView exam_submit = dialog.findViewById(R.id.exam_submit);
        exam_submit.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), ResultActivity.class);
            startActivity(intent);
            dialog.dismiss();
        });
        dialog.show();
        exam_cancle.setOnClickListener(v -> {
            dialog.dismiss();
        });
    }

    @SuppressLint("WrongConstant")
    private void submit_Box() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.test_submit_box, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        Button btn_cancle = dialogView.findViewById(R.id.btn_cancle);
        Button btn_submit = dialogView.findViewById(R.id.btn_submit);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        btn_submit.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), ResultActivity.class);
            startActivity(intent);
            alertDialog.dismiss();
        });
        btn_cancle.setOnClickListener(v -> {
            alertDialog.dismiss();
        });
    }

    public String readJSONDataFromFile() throws IOException {
        InputStream inputStream = null;
        StringBuilder builder = new StringBuilder();

        try {

            String jsonString = null;
            inputStream = getResources().openRawResource(R.raw.test_data);
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream, "UTF-8"));

            while ((jsonString = bufferedReader.readLine()) != null) {
                builder.append(jsonString);
                //Constant.logPrint("fjfjfjfffjfjf", jsonString + "");
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return new String(builder);
    }

    @Override
    public void onClickQuestionItem(QuestionTable clickedItem, int position) {
        mViewModel.updateQuestionTable(clickedItem);
    }


    // PopularCoursesAdapter
    public class PaperAdapter extends RecyclerView.Adapter<PaperAdapter.ViewHolder> {
        List<SubjectDataTable> subjectDataTables;
        List<QuestionTable> questionTables;
        private Context mContext;

        public PaperAdapter(Context context, List<SubjectDataTable> subjectDataTables, List<QuestionTable> questionTables) {
            this.mContext = context;
            this.subjectDataTables = subjectDataTables;
            this.questionTables = questionTables;

        }

        @NonNull
        @Override
        public PaperAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_menu_ques, viewGroup, false);
            return new PaperAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull PaperAdapter.ViewHolder holder, int position) {
            holder.txt_ques.setText(subjectDataTables.get(position).getSubjectName());
            holder.im_arrow_down.setOnClickListener(v -> {
                holder.im_arrrow_up.setVisibility(View.VISIBLE);
                holder.sub_recyler.setVisibility(View.GONE);
                holder.im_arrow_down.setVisibility(View.GONE);
            });
            holder.im_arrrow_up.setOnClickListener(v -> {
                holder.im_arrow_down.setVisibility(View.VISIBLE);
                holder.sub_recyler.setVisibility(View.VISIBLE);
                holder.im_arrrow_up.setVisibility(View.GONE);
            });
            holder.setIsRecyclable(false);
        }

        @Override
        public int getItemCount() {
            return subjectDataTables.size();

        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView im_arrow_down, im_arrrow_up;
            TextView txt_ques;
            RecyclerView sub_recyler;
            LinearLayout sub_layout;

            public ViewHolder(@NonNull View view) {
                super(view);
                sub_layout = view.findViewById(R.id.sub_layout);
                txt_ques = view.findViewById(R.id.txt_ques);
                im_arrow_down = view.findViewById(R.id.im_arrow_down);
                im_arrrow_up = view.findViewById(R.id.im_arrrow_up);
                sub_recyler = view.findViewById(R.id.sub_recyler);
                GridLayoutManager lm = new GridLayoutManager(mContext, 5);
                sub_recyler.setLayoutManager(lm);

                ExamListAdapter examListAdapter = new ExamListAdapter(mContext, questionTables);
                sub_recyler.setAdapter(examListAdapter);
            }
        }
    }

    public static class ExamListAdapter extends RecyclerView.Adapter<ExamListAdapter.ViewHolder> {
        Context context;
        List<QuestionTable> questionTables;


        public ExamListAdapter(Context context, List<QuestionTable> questionTables) {
            this.context = context;
            this.questionTables = questionTables;
            //this.answerLists=answerLists;
        }

        @NonNull
        @Override
        public ExamListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.exam_list_layout, viewGroup, false);
            return new ExamListAdapter.ViewHolder(view);
        }

        @SuppressLint({"WrongConstant", "LongLogTag", "ResourceAsColor"})
        @Override
        public void onBindViewHolder(@NonNull ExamListAdapter.ViewHolder holder, int position) {
            int finalPosition1 = position;
            binding.llReview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    questionTables.get(finalPosition1).setUserQueStatus(Constant.REVIEW);
                    binding.llReview.setVisibility(View.GONE);
                    binding.llReview1.setVisibility(View.VISIBLE);
                }
            });
            binding.llReview1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    questionTables.get(finalPosition1).setUserQueStatus(Constant.UNANSWERED);
                    binding.llReview1.setVisibility(View.GONE);
                    binding.llReview.setVisibility(View.VISIBLE);

                }
            });
            if (questionTables.get(holder.getAdapterPosition()).getUserQueStatus() == Constant.ANSWERED) {
                binding.llReview1.setVisibility(View.GONE);
                binding.llReview.setVisibility(View.VISIBLE);
                holder.exam_ques.setBackgroundResource(R.drawable.answered_circle);
                holder.exam_ques.setTextColor(Color.WHITE);
            } else if (questionTables.get(holder.getAdapterPosition()).getUserQueStatus() == Constant.REVIEW) {
                holder.exam_ques.setBackgroundResource(R.drawable.review_cricle);
                holder.exam_ques.setTextColor(Color.WHITE);
            } else if (questionTables.get(holder.getAdapterPosition()).getUserQueStatus() == Constant.UNANSWERED) {
                holder.exam_ques.setBackgroundResource(R.drawable.not_answered_circle);
                holder.exam_ques.setTextColor(Color.WHITE);
            } else if (questionTables.get(holder.getAdapterPosition()).getUserQueStatus() == Constant.NOT_VISITED) {
                holder.exam_ques.setBackgroundResource(R.drawable.grey_circle);
                holder.exam_ques.setTextColor(Color.BLACK);
            }

            int d = ++position;
            holder.exam_ques.setText(d + "");
            //Constant.logPrint("dposition", d + "");
            int finalPosition = position;

            holder.ll_exam_ques.setOnClickListener(v -> {
                binding.questionRecycler.scrollToPosition(finalPosition - 1);
//                Constant.logPrint("finalPosition", finalPosition + "");
               
                if (binding.drawelayout.isDrawerOpen(Gravity.END)) {
                    binding.drawelayout.closeDrawer(Gravity.END);
                } else {
                    binding.drawelayout.openDrawer(Gravity.END);
                }
            });

        }

        @Override
        public int getItemCount() {
            return questionTables.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView exam_ques;
            LinearLayout ll_exam_ques;

            public ViewHolder(@NonNull View view) {
                super(view);
                ll_exam_ques = view.findViewById(R.id.ll_exam_ques);
                exam_ques = view.findViewById(R.id.exam_ques);
            }
        }


    }

    private void setSnapHelper() {
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(binding.questionRecycler);

        binding.questionRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                View view = snapHelper.findSnapView(recyclerView.getLayoutManager());
                quesId = recyclerView.getLayoutManager().getPosition(view);

                //if (questionTables.get(queNumPosition).getUserQueStatus()==Constant.UNANSWERED)

                binding.tvquestionNo.setText("Q." + String.valueOf(quesId + 1));
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }


    private void startTimer() {
        long totalTime = Long.parseLong(examTime) * 60 * 1000;
        CountDownTimer timer = new CountDownTimer(totalTime + 1000, 1000) {
            @Override
            public void onTick(long remainingTime) {
                String time = String.format("%02d:%02d min",
                        TimeUnit.MILLISECONDS.toMinutes(remainingTime),
                        TimeUnit.MILLISECONDS.toSeconds(remainingTime) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(remainingTime))
                );
                binding.time.setText(time);
            }

            @Override
            public void onFinish() {

            }
        };
        timer.start();
    }



   /* @Override
    public void onBackPressed() {

    }*/
}