package com.example.examapp.studentexam;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.examapp.Constant;
import com.example.examapp.databinding.ActivityResultBinding;

public class ResultActivity extends AppCompatActivity {
    ActivityResultBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityResultBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Constant.setStatusBar(ResultActivity.this);
        binding.back.setOnClickListener(v -> {
            finish();
        });
    }
}